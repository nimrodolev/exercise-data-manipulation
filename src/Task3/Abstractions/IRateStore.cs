using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Task3
{
    public interface IRateStore
    {
        Task<IEnumerable<HotelRate>> SearchRatesAsync(int hotelId, DateTime arrivalDate);
        Task<HashSet<int>> GetAvailableHotelIDsAsync(); 
    }
}