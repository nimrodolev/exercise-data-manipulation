using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Task3
{
    [Route("/hotels")]
    public class HotelsController : Controller
    {
        IRateStore _rates;
        public HotelsController(IRateStore rates)
        {
            _rates = rates;
        }
        [HttpGet("{hotelId}")]
        public async Task<IActionResult> Search([FromRoute]int hotelId, [FromQuery]DateTime arrivalDate)
        {
            var availableHotelIDs = await _rates.GetAvailableHotelIDsAsync();
            if(!availableHotelIDs.Contains(hotelId))
                return NotFound($"No hotel with ID {hotelId} was found");
            var res = _rates.SearchRatesAsync(hotelId, arrivalDate);
            return Json(res);
        }
    }
}