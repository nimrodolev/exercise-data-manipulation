using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Task3
{
    public class DummyRateStore : IRateStore
    {
        private Dictionary<int, Dictionary<DateTime, List<HotelRate>>> _cache = null;
        private SemaphoreSlim _lock = new SemaphoreSlim(1, 1);
        private string _ratesFileLocation;

        public DummyRateStore(string ratesFileLocation)
        {
            _ratesFileLocation = ratesFileLocation;
        }

        public async Task<HashSet<int>> GetAvailableHotelIDsAsync()
        {
            await EnsureCacheLoadedAsync();
            return _cache.Keys.ToHashSet();
        }

        public async Task<IEnumerable<HotelRate>> SearchRatesAsync(int hotelId, DateTime arrivalDate)
        {
            var date = arrivalDate.Date;
            await EnsureCacheLoadedAsync();
            Dictionary<DateTime, List<HotelRate>> ratesByDate = null;
            if (!_cache.TryGetValue(hotelId, out ratesByDate))
                throw new KeyNotFoundException($"No hotel with ID {hotelId} was found");
            List<HotelRate> ratesForDate = null;
            if (!ratesByDate.TryGetValue(date, out ratesForDate))
                return new HotelRate[0];
            
            return ratesForDate;
        }

        private async Task EnsureCacheLoadedAsync()
        {
            if (_cache != null)
                return;
            try
            {
                await _lock.WaitAsync();
                if (_cache != null)
                    return;
                _cache = LoadRates();
            }
            finally
            {
                _lock.Release();
            }
        }

        private Dictionary<int, Dictionary<DateTime, List<HotelRate>>> LoadRates()
        {   
            var result = new Dictionary<int, Dictionary<DateTime, List<HotelRate>>>(); 
            foreach (var hotel in ReadRatesFile(_ratesFileLocation))
            {
                var ratesByDate = result[hotel.HotelDetails.HotelID] = new Dictionary<DateTime, List<HotelRate>>();
                foreach (var rate in hotel.HotelRates)
                {
                    var startDate = rate.TargetDay.Date;
                    List<HotelRate> ratesForThatDay = null;
                    if (!ratesByDate.TryGetValue(startDate, out ratesForThatDay))
                        ratesForThatDay = ratesByDate[startDate] = new List<HotelRate>();
                    ratesForThatDay.Add(rate);
                }
            }
            return result;
        }

        private IEnumerable<Hotel> ReadRatesFile(string fileLocation)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            var serializer = JsonSerializer.Create(jsonSerializerSettings);
            var shouldRead = false;
            using (StreamReader sr = new StreamReader(File.OpenRead(fileLocation)))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                while (reader.Read())
                {
                    //skip the first array opening
                    if (!shouldRead && reader.TokenType == JsonToken.StartArray)
                            continue;
                    else
                    {
                        shouldRead = true;
                        if (reader.TokenType == JsonToken.StartObject)
                        {
                            var rate = serializer.Deserialize<Hotel>(reader);
                            yield return rate;
                        }
                    }

                }
            }
        }
    }
}