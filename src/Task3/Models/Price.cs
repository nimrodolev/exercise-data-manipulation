using System;

namespace Task3
{    public class Price
    {
        public double NumericFloat { get; set; }
        public string Currency { get; set; }
    }
}