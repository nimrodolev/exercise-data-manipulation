using System;
using Newtonsoft.Json;

namespace Task3
{
    public class Hotel
    {
        [JsonProperty("hotel")]
        public HotelDetails HotelDetails { get; set; }
        public HotelRate[] HotelRates { get; set; }
    }
}