using System;

namespace Task3
{
    public class HotelRate
    {
        public int Adults { get; set; }
        public int LOS { get; set; }
        public Price Price { get; set; }
        public string RateName { get; set; }
        public RateTag[] RateTags { get; set; }
        public DateTime TargetDay { get; set; }
    }
}