using System;

namespace Task3
{
    public class RateTag
    {
        public string Name { get; set; }
        public bool Shape { get; set; }
    }
}