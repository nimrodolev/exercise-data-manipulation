using System;

namespace Task3
{
    public class HotelDetails
    {
        public int HotelID { get; set; }
        public string Name { get; set; }
        public double ReviewScore { get; set; }
        public int Classification { get; set; }
    }
}