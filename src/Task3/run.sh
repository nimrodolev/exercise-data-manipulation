echo "#########################################################"
echo "Bulding project"
echo "#########################################################"
dotnet build ./Task3.csproj
echo "#########################################################"
echo "Starting web server. After it has started, access it using address http://localhost:5000/hotels/7294?arrivalDate=2016-03-15"
echo "#########################################################"
dotnet ./bin/Debug/netcoreapp2.2/Task3.dll --environment Development
echo "#########################################################"
echo "Bye bye (:"
echo "#########################################################"