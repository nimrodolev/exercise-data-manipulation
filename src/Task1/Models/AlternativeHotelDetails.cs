using System;

namespace Task1
{
    public class AlternativeHotelDetails
    {
        public string Name { get; set; }
        public double AvgReviewScore { get; set; }
        public int NumOfReviews { get; set; }
        public string ShortDescription { get; set; }
    }
}