using System;

namespace Task1
{
    public class HotelDetails
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int NumOfStars { get; set; }
        public double AvgReviewScore { get; set; }
        public int NumOfReviews { get; set; }
        public string Description { get; set; }
        public string[] RoomCategories { get; set; }
        public AlternativeHotelDetails[] AlternativeHotels { get; set; }
    }
}