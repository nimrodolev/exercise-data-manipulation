using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace Task1
{
    public class BookingScraper
    {
        #region constants
        private const string TITLE_SPAN_ID = "hp_hotel_name";
        private const string ADDR_SPAN_ID = "hp_address_subtitle";
        private const string STARS_CLASS_NAME = "stars";
        private const string STARS_RATING_CLASS_PREFIX = "ratings_stars_";
        private const string RATING_CLASS_NAME = "average";
        private const string REVIEW_CLASS_NAME = "score_from_number_of_reviews";
        private const string DESCRIPTION_ID = "summary";
        private const string ROOM_CATEGORY_CLASS = "ftd";
        private const string ALTERNATIVE_CELL_CLASS = "althotelsCell";
        private const string ALTERNATIVE_HOTEL_NAME_CLASS = "althotel_link";
        private const string ALTERNATIVE_HOTEL_DESC_CLASS = "hp_compset_description";
        private const string ALTERNATIVE_REVIEW_COUNT_CLASS = "count";
        private const string ALTERNATIVE_HOTEL_AVG_REVIEW_CLASS = "average";

        #endregion

        public bool TryScrapePage(Stream page, out HotelDetails details)
        {
            details = null;
            var doc = new HtmlDocument();
            doc.Load(page);
            
            if (!TryGetInnerHtmlByID(doc, TITLE_SPAN_ID, out string title))
                return false;
            if (!TryGetInnerHtmlByID(doc, ADDR_SPAN_ID, out string address))
                return false;
            if(!TryGetNumOfStars(doc, out int stars))
                return false;
            if(!TryGetNumOfReviews(doc, out int reviews))
                return false;
            if(!TryGetRating(doc, out double rating))
                return false;
            if (!TryGetDescription(doc, out string description))
                return false;
            if (!TryGetAlternativeHotels(doc, out AlternativeHotelDetails[] alternatives))
                return false;
            var roomCategories = GetRoomCategories(doc);

            details = new HotelDetails
            {
                Name = title,
                Address = address,
                NumOfStars = stars,
                NumOfReviews = reviews,
                AvgReviewScore = rating,
                Description = description,
                RoomCategories = roomCategories,
                AlternativeHotels = alternatives
            };

            return true;
        }

        private bool TryGetInnerHtmlByID(HtmlDocument doc, string ID, out string result)
        {
            result = null;
            var element = doc.GetElementbyId(ID);
            if (element == null)
                return false;
            result = element.InnerText.Trim();
            return true;
        }

        private bool TryGetNumOfStars(HtmlDocument doc, out int result)
        {
            result = 0;
            var element = doc.DocumentNode
                              .Descendants()
                              .FirstOrDefault(e => e.HasClass(STARS_CLASS_NAME));
            if (element == null)
                return false;
            
            var starNumClasses = element.GetClasses()
                                         .Where(s => s.StartsWith(STARS_RATING_CLASS_PREFIX))
                                         .ToArray();
            if (starNumClasses.Length != 1)
                return false;
            var starNumStr = starNumClasses[0].Substring(STARS_RATING_CLASS_PREFIX.Length);
            return int.TryParse(starNumStr, out result);
        }

        private bool TryGetRating(HtmlDocument doc, out double result)
        {
            result = 0;
            var ratingsElem = doc.DocumentNode
                             .Descendants()
                             .Where(e => e.HasClass(RATING_CLASS_NAME))
                             .FirstOrDefault();
            if (ratingsElem == null)
                return false;
            return double.TryParse(ratingsElem.InnerText, out result);
        }
    
        private bool TryGetNumOfReviews(HtmlDocument doc, out int result)
        {
            result = 0;
            var elem = doc.DocumentNode
                          .Descendants()
                          .Where(e => e.HasClass(REVIEW_CLASS_NAME))
                          .FirstOrDefault();
            if (elem == null)
                return false;
            var strong = elem.Descendants("strong").FirstOrDefault();;
            if (strong == null)
                return false;
            return int.TryParse(strong.InnerText, out result);
        }

        private bool TryGetDescription(HtmlDocument doc, out string result)
        {
            result = null;
            var sum = doc.GetElementbyId(DESCRIPTION_ID);
            if(sum == null)
                return false;
            var paragraphs = sum.Descendants("p").ToArray();
            if (paragraphs.Length == 0)
                return false;
            var sb = new StringBuilder();
            foreach (var p in paragraphs)
                sb.AppendLine(p.InnerText.Trim());
            
            result = sb.ToString();
            return true;
        }

        private string[] GetRoomCategories(HtmlDocument doc)
        {
            var result = doc.DocumentNode
                            .Descendants()
                            .Where(e => e.HasClass(ROOM_CATEGORY_CLASS))
                            .Select(e => e.InnerText.Trim())
                            .ToArray();
            return result;
        }

        private bool TryGetAlternativeHotels(HtmlDocument doc, out AlternativeHotelDetails[] result)
        {
            var alternativesCells = doc.DocumentNode
                                       .Descendants()
                                       .Where(e => e.HasClass(ALTERNATIVE_CELL_CLASS))
                                       .ToArray();
            result = new AlternativeHotelDetails[alternativesCells.Length];
            for (int i = 0; i < result.Length; i++)
            {
                var cell = alternativesCells[i];
                AlternativeHotelDetails details;
                if (!TryGetAlternativeHotelData(cell, out details))
                    return false;
                result[i] = details;
            }
            return true;
        }

        private bool TryGetAlternativeHotelData(HtmlNode node, out AlternativeHotelDetails result)
        {
            result = null;
            var searching = new Dictionary<string, string>
            {
                [ALTERNATIVE_HOTEL_NAME_CLASS] = string.Empty,
                [ALTERNATIVE_HOTEL_DESC_CLASS] = string.Empty,
                [ALTERNATIVE_REVIEW_COUNT_CLASS] = string.Empty,
                [ALTERNATIVE_HOTEL_AVG_REVIEW_CLASS] = string.Empty
            };

            foreach (var el in node.Descendants())
            {
                var classes = el.GetClasses();
                foreach (var cl in classes)
                {
                    if (searching.ContainsKey(cl))
                    {
                        searching[cl] = el.InnerText;
                        break;
                    }
                }
            }
            
            if (string.IsNullOrEmpty(searching[ALTERNATIVE_HOTEL_NAME_CLASS]) || string.IsNullOrEmpty(searching[ALTERNATIVE_HOTEL_DESC_CLASS]))
                return false;
            int reviews = 0;
            double score = 0;
            if (!int.TryParse(searching[ALTERNATIVE_REVIEW_COUNT_CLASS], out reviews))
                return false;
            if (!double.TryParse(searching[ALTERNATIVE_HOTEL_AVG_REVIEW_CLASS], out score))
                return false;

            result = new AlternativeHotelDetails
            {
                Name = searching[ALTERNATIVE_HOTEL_NAME_CLASS].Trim(),
                ShortDescription = searching[ALTERNATIVE_HOTEL_DESC_CLASS].Trim(),
                NumOfReviews = reviews,
                AvgReviewScore = score
            };
            return true;
        }
    }
}