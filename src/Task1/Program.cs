﻿using System;
using System.IO;
using Microsoft.Extensions.CommandLineUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.Name = "Booking.com Data Scraper - Task 1";
            app.HelpOption("-?|-h|--help");
            var inputOption = app.Option("-i|--input <file-path>", "The path to the HTML file to be scraped", CommandOptionType.SingleValue);
            var outputOption = app.Option("-o|--output <file-path>", 
                                          "A file path to write the result into. If not specified, output prints to stdout.", 
                                          CommandOptionType.SingleValue);
            var prettyOption = app.Option("-p|--pretty", "Pretty print the output JSON", CommandOptionType.NoValue);


            
            app.OnExecute(() =>
            {
                if (!inputOption.HasValue() || !File.Exists(inputOption.Value()))
                {
                    PrintError("Must pass an existing HTML file to be scraped");
                    return 1;
                }
                var inputFile = inputOption.Value();
                var scraper = new BookingScraper();
                HotelDetails details;
                if (!scraper.TryScrapePage(File.OpenRead(inputFile), out details))
                {
                    PrintError($"Failed to scrape data from file {inputFile}");
                    return 2;
                }
                var formatting = prettyOption.HasValue() ? Formatting.Indented : Formatting.None;
                var outputStr = JObject.FromObject(details).ToString(formatting);
                if (outputOption.HasValue())
                {
                    var outputFile = outputOption.Value();
                    File.WriteAllText(outputFile, outputStr);
                    Console.WriteLine($"Results JSON written into {outputFile}");
                }
                else
                {
                    Console.WriteLine(outputStr);
                }
                return 0;
            });
            app.Execute(args);
        }

        private static void PrintError(string msg)
        {
            var original = Console.ForegroundColor;
            if (original != ConsoleColor.Red)
                Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            if (Console.ForegroundColor != original)
                Console.ForegroundColor = original;
        }
    }
}
