﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Microsoft.Extensions.CommandLineUtils;
using Newtonsoft.Json;

namespace Task2
{
    class Program
    {
         static void Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.Name = "Rates file transformer - Task 2";
            app.HelpOption("-?|-h|--help");
            var inputOption = app.Option("-i|--input <file-path>", "The path to the rates JSON file", CommandOptionType.SingleValue);
            var outputOption = app.Option("-o|--output <file-path>", 
                                          "A file path to write the result into", 
                                          CommandOptionType.SingleValue);

            
            app.OnExecute(() =>
            {
                if (!inputOption.HasValue() || !File.Exists(inputOption.Value()))
                {
                    PrintError("Must pass an file as input");
                    return 1;
                }
                var inputFile = inputOption.Value();
                var outputFile = outputOption.HasValue() ? outputOption.Value() : Path.Combine(Directory.GetCurrentDirectory(), "output.csv");

                IEnumerable<HotelRateInput> dataStream = ReadRates(File.OpenRead(inputFile));
                IEnumerable<HotelRateOutput> transformedStream = dataStream.Select(ConvertRateFormat);
                WriteRatesToCSV(transformedStream, File.OpenWrite(outputFile));
                Console.WriteLine($"CSV output written into {outputFile}");
                return 0;
            });
            app.Execute(args);
        }

        public static HotelRateOutput ConvertRateFormat(HotelRateInput input)
        {
            var hasBreakfast = input.RateTags.FirstOrDefault(t => t.Name.ToLower() == "breakfast")?.Shape ?? false;
            return new HotelRateOutput
            {
                ADULTS = input.Adults,
                CURRENCY = input.Price.Currency,
                PRICE = input.Price.NumericFloat,
                BREAKFAST_INCLUDED = hasBreakfast ? 1 : 0,
                RATENAME = input.RateName,
                ARRIVAL_DATE = input.TargetDay.Date,
                DEPARTURE_DATE = input.TargetDay.Date.AddDays(input.LOS)
            };
        }
        public static void WriteRatesToCSV(IEnumerable<HotelRateOutput> rates, Stream targetStream)
        {
            using (var streamWriter = new StreamWriter(targetStream))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.Configuration.RegisterClassMap<HotelRateOutputClassMap>();
                csvWriter.WriteRecords(rates);
            }
        }
        public static IEnumerable<HotelRateInput> ReadRates(Stream data)
        {
            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            var serializer = JsonSerializer.Create(jsonSerializerSettings);
            var shouldRead = false;
            using (StreamReader sr = new StreamReader(data))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                while (reader.Read())
                {
                    //skip everything until we get to into the rates array
                    if (!shouldRead)
                    {
                        if (reader.TokenType == JsonToken.StartArray)
                        {
                            shouldRead = true;
                            continue;
                        }
                    }
                    else
                    {
                        if (reader.TokenType == JsonToken.StartObject)
                        {
                            var rate = serializer.Deserialize<HotelRateInput>(reader);
                            yield return rate;
                        }
                    }

                }
            }
        }
        private static void PrintError(string msg)
        {
            var original = Console.ForegroundColor;
            if (original != ConsoleColor.Red)
                Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(msg);
            if (Console.ForegroundColor != original)
                Console.ForegroundColor = original;
        }
    }
}
