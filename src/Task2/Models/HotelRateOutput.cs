using System;

namespace Task2
{
    public class HotelRateOutput
    {
        public DateTime ARRIVAL_DATE { get; set; }
        public DateTime DEPARTURE_DATE { get; set; }
        public double PRICE { get; set; }
        public string CURRENCY { get; set; }
        public string RATENAME { get; set; }
        public int ADULTS { get; set; }
        public int BREAKFAST_INCLUDED { get; set; }
    }
}