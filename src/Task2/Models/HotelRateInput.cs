using System;

namespace Task2
{
    public class HotelRateInput
    {
        public int Adults { get; set; }
        public int LOS { get; set; }
        public Price Price { get; set; }
        public string RateName { get; set; }
        public RateTag[] RateTags { get; set; }
        public DateTime TargetDay { get; set; }
    }
    public class RateTag
    {
        public string Name { get; set; }
        public bool Shape { get; set; }
    }

    public class Price
    {
        public double NumericFloat { get; set; }
        public string Currency { get; set; }
    }
}