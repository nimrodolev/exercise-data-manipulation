using System;
using CsvHelper;
using CsvHelper.Configuration;

namespace Task2
{
    internal class HotelRateOutputClassMap : ClassMap<HotelRateOutput>
    {
			public HotelRateOutputClassMap()
			{
				AutoMap();
				Map(r => r.ARRIVAL_DATE).TypeConverterOption.Format("dd.MM.yy");
				Map(r => r.DEPARTURE_DATE).TypeConverterOption.Format("dd.MM.yy");
			}
    }
}