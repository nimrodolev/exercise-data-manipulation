## Task 1: Web Extraction
### Description
Write a data extractor that reads out content from extraction.booking.html and
extracts the below listed information. The attached screen capture (screencapture-
www-bookingcom.png) explains the colors below:
1. Hotel name (red)
2. Address (red)
3. Classification / stars (red)
4. Review points (pink)
5. Number of reviews (pink)
6. Description (blue)
7. Room categories (green)
8. Alternative hotels (yellow)

### Input
String, stream or file with html content taken from extraction.booking.html.
### Output
Required output format: JSON string.


## Task 2: Reporting
### Description
Generate an excel report based on raw data (hotelrates.json) that has a similar structure to the screen capture (screencapture-excel-report.png)

`LOS` = length of stay (number of room nights)

The column `BREAKFAST_INCLUDED` is a Boolean variable (1: breakfast included, 0: breakfast excluded)

### Input
String, stream, JsonObject or file based on the file hotelrates.json
### Output
Required output format: Excel file showing the look and feel screen capture.

*Optional*: Suggest an architecture on how to automate the process that an email is sent at at time x attaching the report (frameworks, libraries, ready made solutionsetc.: free of choice)

## Task 3: Web Services / API
### Description
Write a RESTful web service including the parameters HotelID and ArrivalDate. The web service `GET` request imports the file (hotelsrates.json), filters the list by means of the parameters and returns a filtered list.

### Input
Method -> String, stream, JsonObject or file based on the file hotelsrates.json
\
Browser -> HotelID and ArrivalDate

### Output
Output is a HTTP response with a JSON string body